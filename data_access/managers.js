(function() {

    function getManagerByQuery(query, options, helpers, cb) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Managers = dbClient.collection('managers');
                Managers.find(query).toArray(function(retrievalErr, managers) {
                    console.log(retrievalErr)
                    if (!retrievalErr) {

                        helpers.execute(cb, [null, managers]);
                    } else {
                        helpers.execute(cb, [retrievalErr, null]);
                    }
                });

            }
             else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    exports.Managers = {
        getManagerByQuery: getManagerByQuery
    }

})();