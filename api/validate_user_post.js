var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var cookieParser = require('cookie-parser');
var session = require('express-session');
var getDbclient = require('../helpers')
var app = express()
var router = express.Router();

app.use(cookieParser());

app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: !true }
}));

router.use(passport.initialize());
router.use(passport.session());

passport.serializeUser(function(user, done) {
    console.log('serialize User' + JSON.stringify(user));
    done(null, user);
});

//deserializer
passport.deserializeUser(function(user, done) {
    console.log('deserializer', user)
    done(null, user);
});


passport.use(new LocalStrategy(
    function(username, password, done) {
        getDbclient(function(err, db) {
            db.collection('managers').find({ username: username }).toArray().then((user) => {
                console.log(user);
                if (err) { return done(err); }
                if (user.length === 0) {
                    console.log('user not find');
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (user[0].password !== password) {
                    console.log('password not matched');
                    return done(null, false, { message: 'Incorrect password.' });
                }
                console.log('success' + JSON.stringify(user[0]));
                return done(null, user[0]);
            }, (err) => {
                throw err;
            });
            db.close();
        });
    }));

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/api/success',
        failureRedirect: '/api/failure'
        //   failureFlash: true
    })
);

router.get('/success', (req, res) => {
    res.status(200).send('login successful');
});

router.get('/failure', (req, res) => {
    res.status(401).send('Invalid');
});

module.exports = router;